<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * @Route("/about", name="about")
     */
    public function about()
    {
        return $this->render('page/about.html.twig');
    }

    /**
     * @Route("/team", name="team")
     */
    public function team()
    {
        return $this->render('page/team.html.twig');
    }

    /**
     * @Route("/using", name="using")
     */
    public function using()
    {
        return $this->render('page/using.html.twig');
    }
}

<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Category;
use App\Form\ProductType;
use Cocur\Slugify\Slugify;
use App\Repository\ProductRepository;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    /**
     * @Route("/", name="products")
     * @param ProductRepository $repository
     * @param CategoryRepository $repository2
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function products(ProductRepository $repository, CategoryRepository $repository2, PaginatorInterface $paginator, Request $request):Response
    {
        $produits = $repository->findBy(
            [],
            ['promotion' => 'DESC', 'created' => 'DESC']
        );
        $produits = $paginator->paginate(
            $produits,
            $request->query->getInt('page', 1),
            6
        );
        $categories = $repository2->findAll(

        );

        return $this->render('product/index.html.twig', ['produits' => $produits, 'categories' => $categories]);
    }

    /**
     * @Route("/{id}", name="productsByCat")
     * @param ProductRepository $repository
     * @param CategoryRepository $repository2
     * @param Category $category
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function productsByCat(ProductRepository $repository, CategoryRepository $repository2, Category $category, PaginatorInterface $paginator, Request $request):Response
    {
         
        $produits = $repository->findBy(
            ['category' => $category],
            ['promotion' => 'DESC', 'created' => 'DESC']
        );
        $produits = $paginator->paginate(
            $produits,
            $request->query->getInt('page', 1),
            6
        );

        $categories = $repository2->findAll(

        );

        return $this->render('product/index.html.twig', ['produits' => $produits, 'categories' => $categories]);
    }

    /**
     * @Route("/product/{slug}", name="product")
     * @param Product $product
     * @return Response
     */
    public function product(Product $product): Response
    {
        return $this->render('product/detail.html.twig', ['product' => $product]);
    }

    /**
     * @Route("/delproduct/{slug}", name="del_prod")
     *
     * @param Product $product
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delProduct(Product $product, EntityManagerInterface $manager): Response {
        $manager->remove($product);
        $manager->flush();
        return $this->redirectToRoute('products');
    }

    /**
     * @Route("/newproduct", name="new_prod")
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function newProduct(Request $request, EntityManagerInterface $manager): Response
    {
        $product = new Product;
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            
            if(empty($product->getImage())) $product->setImage('visuel_non_disponible1.png');
            
            
            $slugify = new Slugify();
            $product->setSlug($slugify->slugify($product->getNom()));
            $manager->persist($product);
            $manager->flush();
            $this->addFlash(
                'success',
                'Le produit a été correctement ajouté'
            );
            return $this->redirectToRoute('products');
        }
        return $this->render('product/add.html.twig', [
            'form' => $form->createView()
        ]);
    }
}

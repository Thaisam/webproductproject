<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class CategoryFixtures extends Fixture
{
    private $categories = ['Figurine', 'Pull', 'Goodies', 'Casquette', 'Insolite', 'livre'];

    public function load(ObjectManager $manager)
    {
       foreach($this->categories as $category){
            $cat = new Category();
            $cat->setName($category);
            $manager->persist($cat);
        }

        $manager->flush();
    }
}
